package com.jcl.test.delivery;

import com.jcl.test.delivery.data.dto.DeliveryDto;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListContract;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListPresenter;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListUseCase;
import com.jcl.test.delivery.utils.TestSchedulerProvider;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jaylumba on 11/1/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class DeliveryListPresenterTest {

    @Mock
    DeliveryListContract.View view;

    @Mock
    DeliveryListUseCase useCase;

    DeliveryListPresenter presenter;

    @Before
    public void setUp(){
        presenter = new DeliveryListPresenter(new TestSchedulerProvider(), view, useCase);
    }

    @Test
    public void shouldDisplayDeliveryListFromApi() {
        List<DeliveryDto> result = new ArrayList<>();
        result.add(new DeliveryDto("asd","adsad",null));
        when(useCase.retrieveDeliveryListApi()).thenReturn(Single.just(result));
        presenter.loadDeliveriesFromApi();
        verify(view).displayDeliveriesFromApi(any());
    }

    @Test
    public void shouldComplete(){
        List<DeliveryDto> result = new ArrayList<>();
        result.add(new DeliveryDto("asd","adsad",null));
        when(useCase.retrieveDeliveryListApi()).thenReturn(Single.just(result));
        TestObserver<List<DeliveryDto>> subscriber = TestObserver.create();
        useCase.retrieveDeliveryListApi().toObservable().subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.assertComplete();
        Assert.assertEquals("asd",subscriber.values().get(0).get(0).getDescription());
    }

}
