package com.jcl.test.delivery.ui.deliverylist;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.jcl.test.delivery.R;
import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.data.dto.DeliveryDto;
import com.jcl.test.delivery.data.prefs.Prefs;
import com.jcl.test.delivery.data.prefs.PrefsKey;
import com.jcl.test.delivery.event.DeliveryDetailEvent;
import com.jcl.test.delivery.ui.base.BaseActivity;
import com.jcl.test.delivery.ui.deliverydetail.DeliveryDetailActivity;
import com.jcl.test.delivery.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import es.dmoral.toasty.Toasty;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;
import timber.log.Timber;

public class DeliveryListActivity extends BaseActivity implements DeliveryListContract.View {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.rv_delivery)
    RecyclerView rvDelivery;

    @Inject
    DeliveryListPresenter presenter;

    @Inject
    Picasso picasso;

    @Inject
    Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_list);
        ButterKnife.bind(this);
        initRefreshLayout();
        onLoad();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stop();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        swipeRefresh.setRefreshing(active);
    }

    @Override
    public void displayDeliveriesFromApi(List<Delivery> deliveries) {
        presenter.saveDeliveryFromCache(deliveries);
        presenter.updateIsDataLoaded(true);
        initRecycler(deliveries);
    }

    @Override
    public void displayDeliveriesFromApiDto(List<DeliveryDto> deliveriesDto) {
        presenter.saveDeliveryFromCache(Utils.convertDeliveryDtoListToDeliveryList(deliveriesDto));
        presenter.updateIsDataLoaded(true);
        initRecycler(Utils.convertDeliveryDtoListToDeliveryList(deliveriesDto));
    }

    @Override
    public void displayDeliveriesFromCache(List<Delivery> deliveries) {
        initRecycler(deliveries);
    }

    @Override
    public void displayDeliveryApiError(Throwable throwable) {
        Timber.e(throwable.getMessage());
        if (prefs.getBoolean(PrefsKey.IS_DATA_LOADED.name())) {
            Toasty.error(this, getString(R.string.message_error_loaddeliveriesfromcache), Toast.LENGTH_LONG).show();
            presenter.loadDeliveriesFromCache();
        } else
            Toasty.error(this, getString(R.string.message_error_unabletoloaddeliveries), Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayDeliveryCacheError(Throwable throwable) {
        Timber.e(throwable.getMessage());
        Toasty.error(this, getString(R.string.message_error_unabletoloaddeliveries), Toast.LENGTH_LONG).show();
    }

    private void initRefreshLayout(){
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onLoad();
            }
        });
    }

    private void onLoad(){
        if (isNetworkAvailable()) presenter.loadDeliveriesFromApi();
        else if (prefs.getBoolean(PrefsKey.IS_DATA_LOADED.name()))
            presenter.loadDeliveriesFromCache();
        else Toasty.error(this, getString(R.string.message_error_nointernetonfirstload),
                    Toast.LENGTH_LONG).show();
    }

    private void initRecycler(List<Delivery> deliveries) {
        DeliveryAdapter adapter = new DeliveryAdapter(this, deliveries, picasso,
                (obj, pos, options) -> {
                    EventBus.getDefault().postSticky(new DeliveryDetailEvent(pos,(Delivery) obj));
                    if (options == null) {
                        moveToOtherActivity(DeliveryDetailActivity.class);
                    } else {
                        moveToOtherActivityWithSharedElements(DeliveryDetailActivity.class, options);
                    }
                });
        rvDelivery.setLayoutManager(new LinearLayoutManager(this));
        rvDelivery.setAdapter(new SlideInLeftAnimationAdapter(adapter));
    }
}
