package com.jcl.test.delivery.data.dto;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jayan on 8/23/2017.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeliveryDto {
    @SerializedName("description")
    String description;
    @SerializedName("imageUrl")
    String imageUrl;
    @SerializedName("location")
    LocationDto location;
}
