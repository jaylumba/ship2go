package com.jcl.test.delivery.utils;

import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.data.dto.DeliveryDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jayan on 8/25/2017.
 */

public class Utils {

    public static List<Delivery> convertDeliveryDtoListToDeliveryList(List<DeliveryDto> deliveryDtos) {
        List<Delivery> deliveryList = new ArrayList<>();
        for (DeliveryDto deliveryDto : deliveryDtos) {
            Delivery delivery = new Delivery();
            delivery.setDescription(deliveryDto.getDescription());
            delivery.setImageUrl(deliveryDto.getImageUrl());

            if (deliveryDto.getLocation() != null){
                delivery.setLat(deliveryDto.getLocation().getLat());
                delivery.setLng(deliveryDto.getLocation().getLng());
                delivery.setAddress(deliveryDto.getLocation().getAddress());
            }

            deliveryList.add(delivery);
        }
        return deliveryList;
    }
}
