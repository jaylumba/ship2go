package com.jcl.test.delivery.callback;

import android.support.v4.app.ActivityOptionsCompat;

/**
 * Created by jayan on 8/25/2017.
 */
public interface OnListItemClickWithSharedElementListener {
    public void onItemClick(Object obj, int pos, ActivityOptionsCompat options);
}
