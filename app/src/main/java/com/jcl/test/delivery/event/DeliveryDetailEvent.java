package com.jcl.test.delivery.event;

import com.jcl.test.delivery.data.dao.Delivery;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by jayan on 8/26/2017.
 */

@Data
@AllArgsConstructor
public class DeliveryDetailEvent {
    int position;
    Delivery delivery;
}
