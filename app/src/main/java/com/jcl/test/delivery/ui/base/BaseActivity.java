package com.jcl.test.delivery.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jcl.test.delivery.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by jayan on 8/25/2017.
 */

public class BaseActivity extends AppCompatActivity {

    //    MaterialDialog dialogLoading;
//    FoldingCube drawableLoading = new FoldingCube();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        drawableLoading.setScale(0.6f);
//        drawableLoading.setColor(ColorHelper.getColor(this, R.color.colorAccent));
//        dialogLoading = new MaterialDialog.Builder(this)
//                .content(R.string.please_wait)
//                .progress(true, 0)
//                .build();
//        dialogLoading.getProgressBar().setIndeterminateDrawable(drawableLoading);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
//        drawableLoading.setScale(0.6f);
//        drawableLoading.setColor(ColorHelper.getColor(this,R.color.colorAccent));
//        dialogLoading = new MaterialDialog.Builder(this)
//                .content(R.string.please_wait)
//                .progress(true, 0).build();
//        dialogLoading.getProgressBar().setIndeterminateDrawable(drawableLoading);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            animateToRight(this);
        }
    }

    public void setError(final TextInputLayout textInputLayout, final String message) {
        textInputLayout.setError(message);
        textInputLayout.setErrorEnabled(true);
        textInputLayout.getEditText().requestFocus();
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public boolean isValidEmail(final String email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

//    public void showLoading(){
//        dialogLoading.setContent(R.string.please_wait);
//        dialogLoading.show();
//    }

//    public void showLoading(String content){
//        dialogLoading.setContent(content);
//        dialogLoading.show();
//    }

//    public void dismissLoading(){
//        dialogLoading.hide();
//    }


    /**
     * check network connection availability
     */
    public boolean isNetworkAvailable() {
        boolean isConnected = false;
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mWifi.isConnected()) {
            isConnected = true;
        } else {
            NetworkInfo mData = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mData == null) {
                isConnected = false;
            } else {
                boolean isDataEnabled = mData.isConnected();
                isConnected = isDataEnabled ? true : false;
            }
        }
        return isConnected;
    }

    public void animateToLeft(Activity activity) {
        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public static void animateToRight(Activity activity) {
        activity.overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    public Drawable getImageById(final int id) {
        return ContextCompat.getDrawable(this, id);
    }

    public void setToolbarTitle(final String title) {
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void updateToolbarTitle(final String title) {
        getSupportActionBar().setTitle(title);
    }


    public String getStringResource(int stringId) {
        return getResources().getString(stringId);
    }

    public void moveToOtherActivity(Class clz) {
        startActivity(new Intent(this, clz));
        animateToLeft(this);
    }

    public void moveToOtherActivityWithSharedElements(Class clz, View view, String transitionName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, (View) view, transitionName);
            startActivity(new Intent(this, clz), options.toBundle());
        } else {
            startActivity(new Intent(this, clz));
        }
    }

    public void moveToOtherActivityWithSharedElements(Class clz, ActivityOptionsCompat options) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(new Intent(this, clz), options.toBundle());
        } else {
            startActivity(new Intent(this, clz));
        }
    }
}
