package com.jcl.test.delivery.di;

import com.jcl.test.delivery.ui.deliverydetail.DeliveryDetailActivity;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListActivity;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListModule;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListViewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by jayan on 8/23/2017.
 */

@Module
public abstract class BuildersModule {
    @ContributesAndroidInjector(modules = {DeliveryListViewModule.class, DeliveryListModule.class})
    abstract DeliveryListActivity bindDeliveryListActivity();

    @ContributesAndroidInjector(modules = {})
    abstract DeliveryDetailActivity bindDeliveryDetailActivity();
}
