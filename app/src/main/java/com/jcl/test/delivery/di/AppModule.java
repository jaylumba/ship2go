package com.jcl.test.delivery.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.jcl.test.delivery.BuildConfig;
import com.jcl.test.delivery.DeliveryApp;
import com.jcl.test.delivery.data.api.ApiInterface;
import com.jcl.test.delivery.data.dao.DbHelper;
import com.jcl.test.delivery.data.prefs.Prefs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jayan on 8/23/2017.
 */

@Module
public class AppModule {

    private static final int CONNECTION_TIME_OUT = 60;
    private static final int READ_TIME_OUT = 60;

    @Provides
    @Singleton
    Context provideContext(DeliveryApp application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(DeliveryApp application) {
        /** initialize ok http client */
        File cacheDir = application.getExternalCacheDir();
        if (cacheDir == null) {
            cacheDir = application.getCacheDir();
        }
        final Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);

        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .cache(cache)
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(DeliveryApp application) {
        final Gson gson = new GsonBuilder().setLenient().create();

        final Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.HOST_NAME)
                .client(provideOkHttpClient(application))
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public ApiInterface provideApiInterface(DeliveryApp application) {
        return provideRetrofit(application).create(ApiInterface.class);
    }

    @Provides
    @Singleton
    public DbHelper provideDbHelper(DeliveryApp application) {
        return new DbHelper(application);
    }

    @Provides
    @Singleton
    public Prefs providePrefs(SharedPreferences sharedPreferences) {
        return new Prefs(sharedPreferences);
    }

    @Provides
    @Singleton
    public Picasso providePicasso(DeliveryApp application) {
        return new Picasso.Builder(application)
                .executor(Executors.newSingleThreadExecutor())
                .downloader(new OkHttp3Downloader(provideOkHttpClient(application))).build();

    }
}
