package com.jcl.test.delivery.ui.deliverylist;

import com.jcl.test.delivery.data.api.ApiInterface;
import com.jcl.test.delivery.data.dao.DbHelper;
import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.data.dto.DeliveryDto;
import com.jcl.test.delivery.data.prefs.Prefs;
import com.jcl.test.delivery.data.prefs.PrefsKey;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by jayan on 8/25/2017.
 */

public class DeliveryListUseCase {

    private final ApiInterface apiInterface;
    private final DbHelper dbHelper;
    private final Prefs prefs;

    @Inject
    public DeliveryListUseCase(ApiInterface apiInterface, DbHelper dbHelper, Prefs prefs) {
        this.apiInterface = apiInterface;
        this.dbHelper = dbHelper;
        this.prefs = prefs;
    }

    public Single<List<DeliveryDto>> retrieveDeliveryListApi() {
        return apiInterface.getDeliveries();
    }

    Observable<List<Delivery>> retrieveDeliveryListFromCache(){
        return Observable.fromArray(dbHelper.newSession().getDeliveryDao().loadAll());
    }

    void saveDelivery(List<Delivery> deliveries) {
        Delivery[] deliveryArray = deliveries.toArray(new Delivery[deliveries.size()]);
        dbHelper.newSession().getDeliveryDao().insertOrReplaceInTx(deliveryArray);
    }

    void updateIsDataLoaded(boolean isDataLoaded) {
        prefs.setBoolean(PrefsKey.IS_DATA_LOADED.name(), true);
    }
}
