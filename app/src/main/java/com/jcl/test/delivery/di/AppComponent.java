package com.jcl.test.delivery.di;

import android.content.SharedPreferences;

import com.jcl.test.delivery.DeliveryApp;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by jayan on 8/23/2017.
 */
@Singleton
@Component(modules = {
        /* Use AndroidInjectionModule.class if you're not using support library */
        AndroidSupportInjectionModule.class,
        AppModule.class,
        BuildersModule.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(DeliveryApp application);
        @BindsInstance
        Builder prefs(SharedPreferences sharedPreferences);
        AppComponent build();
    }

    void inject(DeliveryApp app);
}
