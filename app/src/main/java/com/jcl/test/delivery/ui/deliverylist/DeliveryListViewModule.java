package com.jcl.test.delivery.ui.deliverylist;

import dagger.Binds;
import dagger.Module;

/**
 * Created by jayan on 8/25/2017.
 */
@Module
public abstract class DeliveryListViewModule {
    @Binds
    abstract DeliveryListContract.View provideView(DeliveryListActivity deliveryListActivity);
}
