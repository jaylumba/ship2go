package com.jcl.test.delivery.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.jcl.test.delivery.R;
import com.jcl.test.delivery.ui.deliverylist.DeliveryListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.img_logo_label)
    ImageView imgLogoLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        animateLogo();
    }

    private void animateLogo() {
        ViewAnimator
                .animate(imgLogo)
                .dp()
                .translationY(-1000, 0)
                .andAnimate(imgLogoLabel)
                .alpha(0, 0)
                .decelerate()
                .duration(500)
                .thenAnimate(imgLogoLabel)
                .fadeIn()
                .duration(500)
                .onStop(() -> moveToDeliveryListActivity())
                .start();
    }

    private void moveToDeliveryListActivity() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, DeliveryListActivity.class));
            finish();
        }, 1500);
    }
}
