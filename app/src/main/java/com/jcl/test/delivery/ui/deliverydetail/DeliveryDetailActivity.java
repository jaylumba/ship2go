package com.jcl.test.delivery.ui.deliverydetail;

import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jcl.test.delivery.R;
import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.event.DeliveryDetailEvent;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class DeliveryDetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_delivery_image)
    ImageView ivDeliveryImage;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindDrawable(R.drawable.img_placeholder)
    Drawable drawablePlaceholder;

    @Inject
    Picasso picasso;
    private GoogleMap mMap;
    private Delivery delivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_delivery_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        // Initialize toolbar
        initToolbar();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void receivedDeliveryDetailEvent(DeliveryDetailEvent event) {
        delivery = event.getDelivery();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tvDescription.setTransitionName(getResources().getString(R.string.TRANS_DELIVERY_DESC) + event.getPosition());
            ivDeliveryImage.setTransitionName(getResources().getString(R.string.TRANS_DELIVERY_IMAGE) + event.getPosition());
        }

        tvDescription.setText(delivery.getDescription());
        tvAddress.setText(delivery.getAddress());
        picasso.load(delivery.getImageUrl())
                .placeholder(drawablePlaceholder)
                .fit()
                .centerCrop()
                .into(ivDeliveryImage);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());
        String markerSnippet = "";
        try {
            addresses = geocoder.getFromLocation(delivery.getLat(), delivery.getLng(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            markerSnippet = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Add a marker in Delivery Location and move the camera
        LatLng deliveryLocation = new LatLng(delivery.getLat(), delivery.getLng());
        Marker marker = mMap.addMarker(new MarkerOptions().position(deliveryLocation).title(delivery.getAddress()).snippet(markerSnippet));
        marker.showInfoWindow();

        CameraPosition cameraPosition = new CameraPosition.Builder().target(deliveryLocation).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
