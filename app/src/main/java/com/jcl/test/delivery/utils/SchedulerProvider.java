package com.jcl.test.delivery.utils;

import io.reactivex.Scheduler;

/**
 * Created by jaylumba on 11/1/2017.
 */

public interface SchedulerProvider {
    Scheduler ui();
    Scheduler computation();
    Scheduler trampoline();
    Scheduler newThread();
    Scheduler io();
}
