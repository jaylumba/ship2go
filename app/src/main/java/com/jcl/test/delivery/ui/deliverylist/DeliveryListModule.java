package com.jcl.test.delivery.ui.deliverylist;

import com.jcl.test.delivery.utils.AppSchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jayan on 8/25/2017.
 */
@Module
public class DeliveryListModule {
    @Provides
    DeliveryListPresenter provideDeliveryListPresenter(DeliveryListContract.View deliveryView,
                                                       DeliveryListUseCase deliveryListUseCase) {
        return new DeliveryListPresenter(new AppSchedulerProvider(), deliveryView, deliveryListUseCase);
    }
}
