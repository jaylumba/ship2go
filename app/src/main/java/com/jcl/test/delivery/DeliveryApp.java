package com.jcl.test.delivery;

import android.app.Activity;
import android.app.Application;

import com.jcl.test.delivery.data.dao.DbHelper;
import com.jcl.test.delivery.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by jayan on 8/23/2017.
 */

public class DeliveryApp extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Inject
    DbHelper dbHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        /** initialize timber */
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        /** initialize calligraphy */
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        /** initialize dagger*/
        DaggerAppComponent
                .builder()
                .application(this)
                .prefs(this.getSharedPreferences("ship2go.prefs", MODE_PRIVATE))
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        dbHelper.destroy();
    }
}
