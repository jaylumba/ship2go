package com.jcl.test.delivery.data.dao;

import com.jcl.test.delivery.DeliveryApp;

/**
 * Created by jayan on 8/25/2017.
 */

public class DbHelper {
    private final static String DB_NAME = "ship2go.db";
    private static DaoMaster.DevOpenHelper sDevOpenHelper;
    private static DaoMaster sDaoMaster;

    public DbHelper(DeliveryApp app) {
        sDevOpenHelper = new DaoMaster.DevOpenHelper(app, DB_NAME, null);
        sDaoMaster = new DaoMaster(sDevOpenHelper.getWritableDatabase());
    }

    public void destroy() {
        try {
            if (sDaoMaster != null) {
                sDaoMaster.getDatabase().close();
                sDaoMaster = null;
            }

            if (sDevOpenHelper != null) {
                sDevOpenHelper.close();
                sDevOpenHelper = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DaoSession newSession() {
        if (sDaoMaster == null) {
            throw new RuntimeException("sDaoMaster is null.");
        }
        return sDaoMaster.newSession();
    }
}
