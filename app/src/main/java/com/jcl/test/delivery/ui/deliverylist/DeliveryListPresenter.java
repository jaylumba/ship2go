package com.jcl.test.delivery.ui.deliverylist;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.mvp.BasePresenter;
import com.jcl.test.delivery.utils.SchedulerProvider;
import com.jcl.test.delivery.utils.Utils;

import java.util.List;

import io.reactivex.Observable;
import timber.log.Timber;

/**
 * Created by jayan on 8/25/2017.
 */

public class DeliveryListPresenter extends BasePresenter<DeliveryListContract.View>
        implements DeliveryListContract.Presenter {

    private final DeliveryListUseCase deliveryListUseCase;
    private final BehaviorRelay<RequestState> requestStateObserver
            = BehaviorRelay.createDefault(RequestState.IDLE);
    private SchedulerProvider schedulerProvider;

    public DeliveryListPresenter(SchedulerProvider schedulerProvider, DeliveryListContract.View view, DeliveryListUseCase deliveryListUseCase) {
        super(view);
        this.deliveryListUseCase = deliveryListUseCase;
        this.schedulerProvider = schedulerProvider;
        observeRequestState();
    }

    @Override
    public void loadDeliveriesFromApi() {
        addDisposable(deliveryListUseCase.retrieveDeliveryListApi()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe(s -> publishRequestState(RequestState.LOADING))
                .doOnSuccess(s -> publishRequestState(RequestState.COMPLETE))
                .doOnError(t -> publishRequestState(RequestState.ERROR))
                .map(Utils::convertDeliveryDtoListToDeliveryList)
                .subscribe(view::displayDeliveriesFromApi, view::displayDeliveryApiError)
        );

    }

    @Override
    public void loadDeliveriesFromCache(){
        addDisposable(deliveryListUseCase.retrieveDeliveryListFromCache()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe(s -> publishRequestState(RequestState.LOADING))
                .doOnComplete(() -> publishRequestState(RequestState.COMPLETE))
                .doOnError(t -> publishRequestState(RequestState.ERROR))
                .subscribe(view::displayDeliveriesFromCache, view::displayDeliveryCacheError)
        );
    }

    @Override
    public void saveDeliveryFromCache(List<Delivery> deliveries) {
        deliveryListUseCase.saveDelivery(deliveries);
    }

    @Override
    public void updateIsDataLoaded(boolean isDataLoaded) {
        deliveryListUseCase.updateIsDataLoaded(isDataLoaded);
    }

    private void publishRequestState(RequestState requestState) {
        addDisposable(Observable.just(requestState)
                .observeOn(schedulerProvider.ui())
                .subscribe(requestStateObserver));
    }

    private void observeRequestState() {
        requestStateObserver.subscribe(requestState -> {
            switch (requestState) {
                case IDLE:
                    break;
                case LOADING:
                    view.setLoadingIndicator(true);
                    break;
                case COMPLETE:
                    view.setLoadingIndicator(false);
                    break;
                case ERROR:
                    view.setLoadingIndicator(false);
                    break;
            }
        }, Timber::e);
    }
}
