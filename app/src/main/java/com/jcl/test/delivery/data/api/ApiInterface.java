package com.jcl.test.delivery.data.api;

import com.jcl.test.delivery.data.dto.DeliveryDto;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by jayan on 8/23/2017.
 */

public interface ApiInterface {
    @GET("deliveries")
    Single<List<DeliveryDto>> getDeliveries();
}
