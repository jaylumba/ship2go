package com.jcl.test.delivery.data.dto;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jayan on 8/23/2017.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDto {
    @SerializedName("lat")
    double lat;
    @SerializedName("lng")
    double lng;
    @SerializedName("address")
    String address;
}
