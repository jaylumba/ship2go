package com.jcl.test.delivery.ui.deliverylist;

import com.jcl.test.delivery.data.dao.Delivery;
import com.jcl.test.delivery.data.dto.DeliveryDto;

import java.util.List;

/**
 * Created by jayan on 8/25/2017.
 */

public interface DeliveryListContract {

    interface View{
        void setLoadingIndicator(boolean active);
        void displayDeliveriesFromApi(List<Delivery> deliveries);
        void displayDeliveriesFromApiDto(List<DeliveryDto> deliveriesDto);
        void displayDeliveriesFromCache(List<Delivery> deliveries);
        void displayDeliveryApiError(Throwable throwable);
        void displayDeliveryCacheError(Throwable throwable);
    }

    interface Presenter{
        void loadDeliveriesFromApi();
        void loadDeliveriesFromCache();
        void saveDeliveryFromCache(List<Delivery> deliveries);
        void updateIsDataLoaded(boolean isDataLoaded);
    }
}
