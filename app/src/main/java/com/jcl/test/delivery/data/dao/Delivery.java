package com.jcl.test.delivery.data.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by jayan on 8/25/2017.
 */

@Entity(indexes = {
        @Index(value = "description,imageUrl,lat,lng,address", unique = true)
})
public class Delivery {
    @Property
    String description;
    @Property
    String imageUrl;
    @Property
    double lat;
    @Property
    double lng;
    @Property
    String address;
@Generated(hash = 1687739595)
public Delivery(String description, String imageUrl, double lat, double lng,
        String address) {
    this.description = description;
    this.imageUrl = imageUrl;
    this.lat = lat;
    this.lng = lng;
    this.address = address;
}
@Generated(hash = 182838443)
public Delivery() {
}
public String getDescription() {
    return this.description;
}
public void setDescription(String description) {
    this.description = description;
}
public String getImageUrl() {
    return this.imageUrl;
}
public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
}
public double getLat() {
    return this.lat;
}
public void setLat(double lat) {
    this.lat = lat;
}
public double getLng() {
    return this.lng;
}
public void setLng(double lng) {
    this.lng = lng;
}
public String getAddress() {
    return this.address;
}
public void setAddress(String address) {
    this.address = address;
}
}
