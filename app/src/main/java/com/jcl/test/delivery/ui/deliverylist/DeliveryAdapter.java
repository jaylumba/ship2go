package com.jcl.test.delivery.ui.deliverylist;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jcl.test.delivery.R;
import com.jcl.test.delivery.callback.OnListItemClickWithSharedElementListener;
import com.jcl.test.delivery.data.dao.Delivery;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jayan on 8/25/2017.
 */

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.ViewHolder> {
    Activity mActivity;
    List<Delivery> deliveries;
    Picasso picasso;
    OnListItemClickWithSharedElementListener callback;

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.root_view)
        RelativeLayout rltRootView;
        @BindView(R.id.iv_delivery_image)
        ImageView ivDeliveryImage;
        @BindView(R.id.tv_description)
        TextView tvDescription;

        @BindDrawable(R.drawable.img_placeholder)
        Drawable drawablePlaceholder;

        @BindString(R.string.TRANS_DELIVERY_IMAGE)
        String transDeliveryImage;
        @BindString(R.string.TRANS_DELIVERY_DESC)
        String transDeliveryDesc;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public DeliveryAdapter(Activity activity, List<Delivery> deliveries, Picasso picasso, OnListItemClickWithSharedElementListener callback) {
        this.mActivity = activity;
        this.deliveries = deliveries;
        this.picasso = picasso;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_delivery, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (holder != null) {

            holder.tvDescription.setText(deliveries.get(position).getDescription());
            picasso.load(deliveries.get(position).getImageUrl())
                    .placeholder(holder.drawablePlaceholder)
                    .fit()
                    .centerCrop()
                    .into(holder.ivDeliveryImage);

            holder.rltRootView.setOnClickListener(view -> {
                if (callback != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.ivDeliveryImage.setTransitionName(holder.transDeliveryImage + position);
                        holder.tvDescription.setTransitionName(holder.transDeliveryDesc + position);

                        Pair<View, String> v1 = Pair.create(holder.ivDeliveryImage, holder.ivDeliveryImage.getTransitionName());
                        Pair<View, String> v2 = Pair.create(holder.tvDescription, holder.tvDescription.getTransitionName());
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(mActivity, v1, v2);
                        callback.onItemClick(deliveries.get(position), position, options);
                    } else {
                        callback.onItemClick(deliveries.get(position), position, null);
                    }
                }
            });
        } else {

        }
    }

    @Override
    public int getItemCount() {
        return deliveries.size();
    }
}
